// library require
const express = require('express')
const config = require('config')
const morgan = require('morgan')
const apiCrm = require('./api/crm/route')


// start app
const app = express()


app.use(morgan(':method :url :response-time'))


// modules
app.use('/api/crm_new', apiCrm)


// configure listen app with port
app.set('port', config.get('app.port'))
app.listen(app.get('port'), ()=>{
    console.log(`Api listening on port ${app.get('port')}`)
})
