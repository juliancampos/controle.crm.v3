const express = require('express')
const Controller = require('./controller')

const router = express.Router()
const controller = Controller()

router.route('/login')
    .get(controller.login)


module.exports = router