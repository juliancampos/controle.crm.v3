const config = require('config')
const Sequelize = require('sequelize')

const url = config.get('suitecrm.type')     + "://" +
            config.get('suitecrm.user')     + ":" +
            config.get('suitecrm.password') + "@" +
            config.get('suitecrm.host')     + "/" +
            config.get('suitecrm.database')
const sequelize = new Sequelize(url)

const Database = {
    getConnection
}

function getConnection() {
    return sequelize
}

module.exports = function factory() {
    return Database
}