const dao = require('../dao')
const sequelize = require('sequelize')
const Database = require('../database')
const database = Database()

const connection = database.getConnection()

const user = connection.define('user', {
    name: sequelize.STRING,
    username: sequelize.STRING,
    password: sequelize.STRING,
    email: sequelize.STRING
})

const userDao = {
    get,
    save,
    update
}

function get(searchParam, callback) {
    dao.get(user, searchParam, function(result) {
        callback(result)
    })
}

function save(userData, callback) {
    dao.save(user, userData, function(result){
        callback(result)
    })
}

function update(callback) {
    dao.update(user, {password: '456'}, {id: '1'}, function(result){
        callback(result)
    })
}

module.exports = userDao