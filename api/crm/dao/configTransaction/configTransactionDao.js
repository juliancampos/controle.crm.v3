const dao = require('../dao')
const sequelize = require('sequelize')
const Database = require('../database')
const database = Database()

const connection = database.getConnection()

const configTransaction = connection.define('configuration_transaction', {
    type: sequelize.STRING
})

const configTransactionDao = {
    get
}

function get() {
    dao.get(configTransaction)
}

module.exports = configTransactionDao