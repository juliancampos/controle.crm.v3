const dao = {
    get,
    save,
    update
}

function get(model, paramSearch, callback) {
    model.findAll(
        { where: paramSearch})
        .then(result => {
            console.log(result)
            callback(result)
        })
    }

function save(model, data, callback) {
    model.sync({force: true})
        .then(() => {
            callback(model.create(data))
        })
}

function update(model, dataToUpdate, dataWhere, callback) {
    callback(
        model.update(
            dataToUpdate,
            {where: dataWhere}
        )
    )
}

module.exports = dao