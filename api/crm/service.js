const Database = require('./database')
const userDao = require('./dao/user/userDao')
const configTransactionDao = require('./dao/configTransaction/configTransactionDao')

const database = Database()

const Service = {
    getUsers,
    getUser,
    getConfig,
    saveUser,
    updateUser
}

function saveUser(userData, callback) {
    userDao.save(userData, function(result) {
        callback(result)
    })
}

function updateUser(callback) {
    userDao.update(function(result){
        callback(result)
    })
}

function getUser(paramSearch, callback) {
    userDao.get(paramSearch, function(result){
        callback(result)
    })
}


function getConfig(callback) {
    configTransactionDao.get(function(result){
        callback(result)
    })
}


function getUsers(callback) {
    const sql = "select ct.type from cmobi_sankhya_service_test.configuration_transaction ct"
    
/*    const sql = "select a.name " + 
                "from cmobi_suitecrm_db.accounts a inner join cmobi_suitecrm_db.accounts_cstm a_cstm on a.id = a_cstm.id_c " +
                "where a_cstm.numero_cpf_c = '06505464662' " +
                "and a.deleted = '0' "*/

    database.run(sql, function(result) {
        callback(result)
    })
}

module.exports = function factory() {
    return Service
}