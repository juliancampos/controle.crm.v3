const config = require('config')
const Sequelize = require('sequelize')


const url = config.get('suitecrm.type')     + "://" +
            config.get('suitecrm.user')     + ":" +
            config.get('suitecrm.password') + "@" +
            config.get('suitecrm.host')     + "/" +
            config.get('suitecrm.database')
const sequelize = new Sequelize(url)


const Database = {
    closeConnection,
    testConnection,
    run
}


function testConnection(callback) {
    sequelize.authenticate()
        .then(() => {
            callback('Connection has been established successfully.')
        })
        .catch(error => {
            callback('Unable to connect to the database:', error)
        })
}


function run(model, callback){
/*    sequelize.query(sql)
        .then(result => {
            callback(result)
        })
        */

    model.findAll().then(result => {
        console.log(result)
        callback(result)
    })
}


function closeConnection(callback) {
    callback("Connection Closed")
}


module.exports = function factory() {
    return Database
}