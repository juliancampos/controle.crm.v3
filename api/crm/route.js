const express = require('express')
const Controller = require('./controller')


const router = express.Router()
const controller = Controller()


router.route('/users')
    .get(controller.getUsers)

router.route('/user')
    .get(controller.getUser)

router.route('/login')
    .get(controller.login)

router.route('/config')
    .get(controller.getConfig)

router.route('/save_user')
    .get(controller.saveUser)

router.route('/user/update')
    .get(controller.updateUser)


module.exports = router