const Service = require('./service')
const service = Service()

const Controller = {
    getUser,
    getUsers,
    getConfig,
    saveUser,
    login,
    updateUser
}

function login(req, resp) {
    const paramSearch = {username: 'julian.campos', password: '123'}
    service.getUser(paramSearch, function(result) {
        resp.status(200).send(result)
    })
}

function updateUser(req, resp) {
    service.updateUser(function(result){
        resp.status(200).send(result)
    })
}

function saveUser(req, resp) {
    const userData = {name: 'julian', password: '123', email: 'julian@email.com', username: 'julian.campos'}
    service.saveUser(userData, function(result){
        resp.status(201).send(result)
    })
}

function getUser(req, resp) {
    service.getUser({}, function(result){
        resp.status(200).send(result)
    })
}

function getUsers(req, resp) {
    service.getUsers(function(result){
        resp.send(result)
    })
}


function getConfig(req, resp) {
    service.getConfig(function(result){
        resp.send(result)
    })
}
module.exports = function factory() {
    return Controller
}